import Product from '@/apis/Product'


export const getProductsFromApi = ({ commit}) => {
    Product.all()
    .then(res => {
        commit('getProducts', res.data);
    })
}
export const getSingleProductFromApi = ({commit}, product_id) => {
    Product.show(product_id)
    .then(res => {
        commit('getProduct', res.data);
    })
}
