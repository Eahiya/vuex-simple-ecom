
export const setProductToCart = (state, {product, quantity}) => {


    const checkExistProductToCart = state.carts.find(item => {
        return item.product.id === product.id;
    })

    
    if(checkExistProductToCart)
    {
        checkExistProductToCart.quantity = (parseInt(checkExistProductToCart.quantity) + parseInt(quantity))
        return;
    }

    state.carts.push({
        product: product,
        quantity: quantity
    })
}


export const setProductToCartFromApi =  (state, data) => {
    state.carts = data;
}


export const removeCartItem = (state, product)=>{
    state.carts = state.carts.filter(item => {
        return item.product.id !== product.id;
    })
}


export const clearCart = (state) => {
    state.carts = [];
}





