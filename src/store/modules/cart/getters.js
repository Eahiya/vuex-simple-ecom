export const cartProductCount = (state) => {
    return state.carts.length;
}

export const totalPriceFromcart = (state) => {
    var sum = 0;
    state.carts.forEach(ietm => {
        sum += ietm.product.price * ietm.quantity;
    })
    return sum;
}