
import Cart from '@/apis/Cart'

export const addToCart = ({commit, dispatch}, {product, quantity}) => {
    commit('setProductToCart', {product, quantity}),

    dispatch('getNotification', {
        type: 'success',
        message: 'Product Add to cart success'
    }, {root: true})   // here root true main it is a global actions

    Cart.create({
        product_id: product.id,
        quantity: quantity
    })
        
}

export const comeCartItemFromApi = ({commit}) => {
        Cart.all()
        .then(res => {
            commit('setProductToCartFromApi', res.data)
        })
}

export const removeSingleProductFromCart = ({commit, dispatch}, product) => {
    commit('removeCartItem', product)
    dispatch('getNotification', {
        type: 'danger',
        message: 'Product remove from cart'
    }, {root: true})   // here root true main it is a global actions

    Cart.destroy(product.id)
}


export const removeAllFromCart = ({commit}) => {
    commit('clearCart')
    Cart.destroyAll()
}






