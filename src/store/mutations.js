export const setNotification = (state, notification) =>{
        state.notifications.push({
            ...notification,
            id: (Math.random().toString(36) + '-' + Date.now().toString(36)).substr(2)
        })
}

export const removeErMessage = (state, noti) => {
    state.notifications = state.notifications.filter(item => {
        return item.id !== noti.id;
    });
}

