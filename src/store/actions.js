
export const getNotification = ({commit}, notification) => {

    commit('setNotification', notification)
}

export const removeErrorMessage = ({commit}, notification) => {
    commit('removeErMessage', notification)
}