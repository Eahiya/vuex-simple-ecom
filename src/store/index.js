import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import * as actions from '@/store/actions'
import * as mutations from '@/store/mutations'
import * as getters from '@/store/getters'
import state from '@/store/state'

// modules
import product from './modules/product'
import cart from "./modules/cart";

export default new Vuex.Store({
    state,
    getters,
    mutations,
    actions,
    

    modules: {
        product,
        cart
    }

});



