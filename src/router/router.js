import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

import Home from '@/pages/Home'
import ProductDetails from '@/pages/ProductDetails'
const routes = [
    {
        path: '/',
        component: Home,
        name: 'home'
    },
    {
        path: '/product-detail/:id',
        component: ProductDetails,
        name: 'product-detail',
        props: true
    }
]

const router = new VueRouter({
    routes,
    mode: 'history'
})


export default router;



