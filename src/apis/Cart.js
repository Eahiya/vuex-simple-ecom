import Api from './Api'

const endPoint = 'cart'
export default {
    all(){
        return Api.get(endPoint)
    },
    create(data){
        return Api.post(endPoint,data)
    },
    destroy(product_id){
        return Api.delete(`${endPoint}/${product_id}`);
    },
    destroyAll(){
        return Api.delete(`${endPoint}s/delete-all`);
    }
}




