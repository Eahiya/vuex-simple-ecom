import Api from '@/apis/Api'


const endPoint = 'product'

export default {
    all(){
        return Api.get(endPoint)
    },
    show(product_id){
        return Api.get(`${endPoint}/${product_id}`);
    }
}




